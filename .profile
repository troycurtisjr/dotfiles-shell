#!/bin/bash

# This is the common place for all interactive and login shells to source.
# This will be used by all shells and should be sourced by those shells specifically
# from their shell specific locations (so that shell specific items can be included there).

# .shellrc should be sourced by everything
. ~/.shellrc

# Source anything for the profile common to all shells
source_rc_dir ~/.profile.d
