#!/usr/bin/env zsh

# Items for all instances (including non-interactive scripts) should go in ~/.zenv.d or
# ~/.shellrc.d/ (if common with bash)

# Source the common profile information
. ~/.profile

source_rc_dir ~/.zsh.d/custom

