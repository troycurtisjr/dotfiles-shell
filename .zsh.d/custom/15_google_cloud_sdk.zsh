
SDKPTH=${HOME}/usr/src/google-cloud-sdk

if [ -d $SDKPTH ]
then
  source ${SDKPTH}/path.zsh.inc
  source ${SDKPTH}/completion.zsh.inc
fi

unset SDKPTH
