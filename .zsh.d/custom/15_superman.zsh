#!/usr/bin/env zsh

vman() {
  vim -c "SuperMan $*"

  if [ "$?" != "0" ]; then
    echo "No manual entry for $*"
  fi
}

compdef vman="man" 
# This is for bash
#complete -o default -o nospace -F _man vman
