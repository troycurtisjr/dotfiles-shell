if which kubectl &> /dev/null
then
  source <(kubectl completion $(basename $SHELL))
fi
