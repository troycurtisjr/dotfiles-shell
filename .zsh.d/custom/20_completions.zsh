
if [ -d /usr/local/share/zsh-completions ]
then
    fpath=(/usr/local/share/zsh-completions $fpath)
fi

# Extract the static invoke completion scripts so that a python script doesn't
# have to run every time.
if which invoke &> /dev/null; then
  # Invoke tab-completion script to be sourced with the Z shell.
  # Known to work on zsh 5.0.x, probably works on later 4.x releases as well (as
  # it uses the older compctl completion system).
  
  _complete_invoke() {
      # `words` contains the entire command string up til now (including
      # program name).
      #
      # We hand it to Invoke so it can figure out the current context: spit back
      # core options, task names, the current task's options, or some combo.
      #
      # Before doing so, we attempt to tease out any collection flag+arg so we
      # can ensure it is applied correctly.
      collection_arg=''
      if [[ "${words}" =~ "(-c|--collection) [^ ]+" ]]; then
          collection_arg=$MATCH
      fi
      # `reply` is the array of valid completions handed back to `compctl`.
      # Use ${=...} to force whitespace splitting in expansion of
      # $collection_arg
      reply=( $(invoke ${=collection_arg} --complete -- ${words}) )
  }
  
  
  # Tell shell builtin to use the above for completing our given binary name(s).
  # * -K: use given function name to generate completions.
  # * +: specifies 'alternative' completion, where options after the '+' are only
  #   used if the completion from the options before the '+' result in no matches.
  # * -f: when function generates no results, use filenames.
  # * positional args: program names to complete for.
  compctl -K _complete_invoke + -f invoke inv
fi

if which fas &> /dev/null; then
  # Invoke tab-completion script to be sourced with the Z shell.
  # Known to work on zsh 5.0.x, probably works on later 4.x releases as well (as
  # it uses the older compctl completion system).

  _complete_fas() {
      # `words` contains the entire command string up til now (including
      # program name).
      #
      # We hand it to Invoke so it can figure out the current context: spit back
      # core options, task names, the current task's options, or some combo.
      #
      # Before doing so, we attempt to tease out any collection flag+arg so we
      # can ensure it is applied correctly.
      collection_arg=''
      if [[ "${words}" =~ "(-c|--collection) [^ ]+" ]]; then
          collection_arg=$MATCH
      fi
      # `reply` is the array of valid completions handed back to `compctl`.
      # Use ${=...} to force whitespace splitting in expansion of
      # $collection_arg
      reply=( $(fas ${=collection_arg} --complete -- ${words}) )
  }


  # Tell shell builtin to use the above for completing our given binary name(s).
  # * -K: use given function name to generate completions.
  # * +: specifies 'alternative' completion, where options after the '+' are only
  #   used if the completion from the options before the '+' result in no matches.
  # * -f: when function generates no results, use filenames.
  # * positional args: program names to complete for.
  compctl -K _complete_fas + -f fas
fi

if which fpkg &> /dev/null; then
  # Invoke tab-completion script to be sourced with the Z shell.
  # Known to work on zsh 5.0.x, probably works on later 4.x releases as well (as
  # it uses the older compctl completion system).

  _complete_fpkg() {
      # `words` contains the entire command string up til now (including
      # program name).
      #
      # We hand it to Invoke so it can figure out the current context: spit back
      # core options, task names, the current task's options, or some combo.
      #
      # Before doing so, we attempt to tease out any collection flag+arg so we
      # can ensure it is applied correctly.
      collection_arg=''
      if [[ "${words}" =~ "(-c|--collection) [^ ]+" ]]; then
          collection_arg=$MATCH
      fi
      # `reply` is the array of valid completions handed back to `compctl`.
      # Use ${=...} to force whitespace splitting in expansion of
      # $collection_arg
      reply=( $(fpkg ${=collection_arg} --complete -- ${words}) )
  }


  # Tell shell builtin to use the above for completing our given binary name(s).
  # * -K: use given function name to generate completions.
  # * +: specifies 'alternative' completion, where options after the '+' are only
  #   used if the completion from the options before the '+' result in no matches.
  # * -f: when function generates no results, use filenames.
  # * positional args: program names to complete for.
  compctl -K _complete_fpkg + -f fpkg
fi

