
if [ -f /usr/share/fzf/shell/key-bindings.zsh ]
then
  . /usr/share/fzf/shell/key-bindings.zsh
fi

if [ -f /usr/share/zsh/site-functions/fzf ]
then
  . /usr/share/zsh/site-functions/fzf
fi
