
# This file is sourced by all bash shells, put interactive only items
# in ~/.bash_profile.d/*, or if common with zsh, ~/.profile.d/ or ~/.shellrc.d
export PATH="/usr/local/bin:$PATH"

#if [ -n "$PS1" ] && which zsh &> /dev/null
#then
#  export SHELL=$(which zsh)
#  exec zsh "$@"
#fi

. ~/.shellrc

cond_source /etc/bashrc

source_rc_dir ~/.bashrc.d
