#!/bin/bash

# Parse the downloaded awskeys.
if [ -f ~/.aws/credentials ]
then
  export AWS_ACCESS_KEY_ID=$(sed -n '/aws_access_key_id *= *\(.*\)/ { s//\1/; p;}' ~/.aws/credentials)
  export AWS_SECRET_ACCESS_KEY=$(sed -n '/aws_secret_access_key *= *\(.*\)/ { s//\1/; p;}' ~/.aws/credentials)
fi
