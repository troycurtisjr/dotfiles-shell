
# Support for "global" npm, which is actually local.
# If the .npmrc file isn't controlled/available, this may need to be executed:
#
# npm config set prefix "$NPM_PACKAGES"
#

NPM_PACKAGES="${HOME}/.local/npm-packages"

if [ -d "$NPM_PACKAGES" ]
then
  prepend_path "${NPM_PACKAGES}/bin"

  export MANPATH="${MANPATH-$(manpath)}:$NPM_PACKAGES/share/man"
fi
