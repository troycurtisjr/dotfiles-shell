#!/bin/bash

export LD_LIBRARY_PATH="/usr/local/lib:/home/troy/usr/lib"
export PATH="~/usr/bin:~/bin:/opt/nodejs/bin:$PATH:/sbin:/usr/sbin"
export MANPATH="${MANPATH}:~/man"
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig:/usr/local/lib64/pkgconfig:${PKG_CONFIG_PATH}

