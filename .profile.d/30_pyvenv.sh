#!/bin/bash

if [ -d ~/.pyenv/bin ]; then
    export PYENV_ROOT="${HOME}/.pyenv"
    prepend_path ${HOME}/.pyenv/bin
fi

if which pyenv &> /dev/null;
then
  eval "$(pyenv init --path)"
  eval "$(pyenv init -)"
  eval "$(pyenv virtualenv-init -)";
fi
