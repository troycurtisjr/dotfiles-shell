#!/bin/bash

TPUT_COLOR_CODE_BLACK=0
TPUT_COLOR_CODE_RED=1
TPUT_COLOR_CODE_GREEN=2
TPUT_COLOR_CODE_YELLOW=3
TPUT_COLOR_CODE_BLUE=4
TPUT_COLOR_CODE_MAGENTA=5
TPUT_COLOR_CODE_CYAN=6
TPUT_COLOR_CODE_WHITE=7

if tty -s
then
TCOLOR_FG_BLACK=$(tput setaf 0)
TCOLOR_FG_RED=$(tput setaf 1)
TCOLOR_FG_GREEN=$(tput setaf 2)
TCOLOR_FG_YELLOW=$(tput setaf 3)
TCOLOR_FG_BLUE=$(tput setaf 4)
TCOLOR_FG_MAGENTA=$(tput setaf 5)
TCOLOR_FG_CYAN=$(tput setaf 6)
TCOLOR_FG_WHITE=$(tput setaf 7)

TCOLOR_BOLD=$(tput bold) 
TCOLOR_DIM=$(tput dim)
TCOLOR_UNDER_ON=$(tput smul) 
TCOLOR_UNDER_OFF=$(tput rmul) 
TCOLOR_REV_ON=$(tput rev)

TCOLOR_RESET=$(tput sgr0) 
fi

# Hardcoded color escape sequences for posterity.
#BLACK="\[\033[0;30m\]"
#DARK_GRAY="\[\033[1;30m\]"
#BLUE="\[\033[0;34m\]"
#LIGHT_BLUE="\[\033[1;34m\]"
#GREEN="\[\033[0;32m\]"
#LIGHT_GREEN="\[\033[1;32m\]"
#CYAN="\[\033[0;36m\]"
#LIGHT_CYAN="\[\033[1;36m\]"
#RED="\[\033[0;31m\]"
#LIGHT_RED="\[\033[1;31m\]"
#PURPLE="\[\033[0;35m\]"
#LIGHT_PURPLE="\[\033[1;35m\]"
#BROWN="\[\033[0;33m\]"
#YELLOW="\[\033[1;33m\]"
#LIGHT_GRAY="\[\033[0;37m\]"
#WHITE="\[\033[1;37m\]"
#NO_COLOR="\[\033[0m\]"
#

# Some other tput escape sequence examples
# MY_VAR=$(tput setaf {colorcode}) - Set foreground color using ANSI escape
# MY_VAR=$(tput setab {colorcode}) - Set background color using ANSI escape
# MY_VAR=$(tput setb {colorcode}) - Set background color
# MY_VAR=$(tput setf {colorcode}) - Set foreground color
# MY_VAR=$(tput bold) - Set bold mode 
# MY_VAR=$(tput dim) - Set half-bright mode 
# MY_VAR=$(tput smul) - Set underline mode 
# MY_VAR=$(tput rmul) - Exit underline mode 
# MY_VAR=$(tput rev) - Turn on reverse mode 
# MY_VAR=$(tput sgr0) - Turn off all attributes
