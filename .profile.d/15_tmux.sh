
function new-tmux-from-dir-name {
  tmux new-session -As `basename $PWD`
}

alias tnew=new-tmux-from-dir-name
