#!/bin/bash

#Alias definitions
alias la='ls -A'
alias ll='ls -lh'
alias l='ls'
alias h='history'
alias w='cd ~/working'
alias vim='vim -X'
alias vi='vim'
# List all the files of a directory (and subdirectory), with the largest listed first
alias dusk='du -s -k -c * | sort -rn'

alias makes='make -s'

alias ls='ls --color'
