#!/usr/bin/env zsh


gt() {
  gitroot=$(git rev-parse --show-cdup 2> /dev/null)
  if [ -z "$gitroot" ]
  then
    svnroot=$(svn info 2> /dev/null | sed -n '/^Working Copy Root Path/ { s/^[^:]\+:\s*//; p; }')
    if [ -n "$svnroot" ]
    then
      cd "$svnroot"
    else
      svnroot=$(readlink -f $(pwd)) 
      svn_base_uuid=$(svn info "$svnroot" | sed -n '/^Repository UUID/ { s/^[^:]\+:\s*//; p; }')
      if [ -n "$svn_base_uuid" ]
      then
        while [ "$svnroot" =~ "^/[^/]\+" ]
        do
          rootcanidate=$(dirname "$svnroot")
          svnuuid=$(svn info "$rootcanidate" | sed -n '/^Repository UUID/ { s/^[^:]\+:\s*//; p; }')

          if [ "$svn_base_uuid" = "$svnuuid" ]
          then
            svnroot="$rootcanidate"
          else
            break
          fi
        done

        cd "$svnroot"
      fi
    fi
  else
    cd "$gitroot"
  fi
}
