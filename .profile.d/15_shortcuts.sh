
# Display VCS status no matter which type we are in.
st() {
  if git rev-parse --show-toplevel &> /dev/null 
  then
    git status -sb "$@"
  else
    svn status "$@"
  fi
}

if which lsd &>/dev/null; then
  alias ls=lsd
fi

