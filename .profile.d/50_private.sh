#!/bin/bash

# If a private profile directory exists, source it.
if [ -d ~/.profile-private.d ]
then
  source_rc_dir ~/.profile-private.d
fi
