#!/bin/bash

if which starship &>/dev/null;
then
  eval "$(starship init -- $(basename $SHELL))"
else
  #export PS1='[ $(_genps1) ]'" "'$( [ $? == 0 ] || echo "\[$TCOLOR_REV_ON\][$?]\[$TCOLOR_RESET\] " )'"\[$TCOLOR_FG_RED\]\u\[$TCOLOR_FG_MAGENTA\]@\h \[$TCOLOR_FG_CYAN\]\w \[$TCOLOR_FG_YELLOW\]"'$(declare -f __git_ps1 &>/dev/null && __git_ps1 "(%s)")'"\n\
  #\[$TCOLOR_FG_GREEN\]\!\$ \[$TCOLOR_RESET\]"

  export PS1='$( [ $? == 0 ] || echo "\[$TCOLOR_REV_ON\][$?]\[$TCOLOR_RESET\] " )'"\[$TCOLOR_FG_RED\]\u\[$TCOLOR_FG_MAGENTA\]@\h \[$TCOLOR_FG_CYAN\]\w \[$TCOLOR_FG_YELLOW\]"'$(declare -f __git_ps1 &>/dev/null && __git_ps1 "(%s)")'"\n\
  \[$TCOLOR_FG_GREEN\]\!\$ \[$TCOLOR_RESET\]"

  export KUBE_ENV_DISABLE_PROMPT=1

  #export PS1="\[$TCOLOR_FG_RED\]\u\[$TCOLOR_FG_MAGENTA\]@\h \[$TCOLOR_FG_CYAN\]\w \[$TCOLOR_FG_YELLOW\]"'$(declare -f __git_ps1 &>/dev/null && __git_ps1 "(%s)")'"\n\
  #"'$(echo -n "($?)")'" \[$TCOLOR_FG_GREEN\]\!\$ \[$TCOLOR_RESET\]"

  settitle() 
  { 
      echo -ne '\033k'"$@"'\033\'; 
      #' Syntax highlighting fix
  }
  case ${TERM} in
    xterm*|rxvt*|Eterm)
      export PROMPT_COMMAND='echo -ne "\033]0;${HOSTNAME}[$(basename "${PWD}")]\007"'
      ;;
    screen*)
      export PROMPT_COMMAND='declare -f | grep -q settitle && settitle $(echo [$(basename "${PWD}" | sed "s/^\([[:alnum:][:space:].]\{0,25\}\).*$/\1/" )];)'
      #export PROMPT_COMMAND='echo -ne "\033k\033\134\033k$(if [ ${UID} == "0" ]; then echo "root"; else $(if [ ${HOSTNAME} != "malkier" ]; then echo ${HOSTNAME}; fi;); echo [$(basename "${PWD}" | sed "s/^\([[:alnum:][:space:].]\{0,25\}\).*$/\1/" )]; fi;)\033\134"'
      ;;
  esac
fi
