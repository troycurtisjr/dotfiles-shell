#!/usr/bin/env zsh

# Disable the annoying Ctrl-S/Ctrl-Q flow control junk

if [[ -t 0 ]]
then
  stty -ixon
fi
