
function g {
  if [[ $# > 0 ]]; then
    git $@
  else
    git status -sb
  fi
}

# Smarter tab completion for git *sounds* nice, only show 
# unstaged files for git-add, don't show ignored files for status/log/etc,
# but for largish repos...this is unbearably slow. So just use the standard
# tab completion.
__git_files () { 
      _wanted files expl 'local files' _files     
}
