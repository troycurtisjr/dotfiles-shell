#!/bin/bash

#Variable to set up commands that shouldn't be put in the history
export HISTIGNORE="l[sal]:[fb]g:cd:cd ..:[ ]*"
#The & suppresses repeat commands
#the [ \t]* allows you to selectively exclude a command from the history
#by putting a space in front of it
