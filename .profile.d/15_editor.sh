
if which emacs &> /dev/null; then
    EMACS_VER=$(emacs --version | sed -n '/GNU Emacs \([0-9]\+\.[0-9]\+\)/ { s//\1/; p; }')
    EMACS_DUMP_FILE="${HOME}/.emacs.d/.cache/dumps/spacemacs-${EMACS_VER}.pdmp"
fi

if [ -f "$EMACS_DUMP_FILE" ]
then
  export VISUAL="emacs --dump-file=$EMACS_DUMP_FILE -nw"
else
  export VISUAL="vim"
fi

export EDITOR=$VISUAL
alias e="$VISUAL"
export PAGER="less -F"
