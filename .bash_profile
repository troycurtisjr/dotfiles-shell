# .bash_profile

# This file is sourced by bash for interactive and login shells
# .bashrc/.bashrc.d (and .shellrc/.shellrc.d) are sourced by everything, so things
# that scripts might need should go there

# Source the common profile information
. ~/.profile

# Source the "everything bash" file
cond_source ~/.bashrc

# Source bash specific profile information if applicable
source_rc_dir ~/.bashrc_profile.d

